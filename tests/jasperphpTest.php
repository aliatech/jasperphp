<?php

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use JasperPHP\JasperPHP;

class jasperphpTest extends \PHPUnit_Framework_TestCase
{
    protected $executable = "/../src/JasperStarter/bin/jasperstarter";

    public function testCreateInstance()
    {
        $obj = new JasperPHP;
        $this->assertTrue($obj instanceof JasperPHP);
    }

    public function testBasicExample()
    {
      // $this->processExample(__DIR__ .'/../examples/hello_world.jrxml', ['pdf'], ['php_version' => 'hola']);
    }

    public function testArgsExample()
    {
      $this->processExample(__DIR__ .'/../examples/addressbook_xml.jrxml', ['pdf'], ['php_version' => 'hola'], [
        '-t' => 'xml',
        '--data-file' => __DIR__ .'/../examples/addressbook.xml',
        '--xml-xpath' => '/addressbook'
      ]);
    }

    protected function processExample($jrxml, $fmt = ['pdf'], $params = [], $args = [])
    {
      $jasper = new JasperPHP;
      $o = pathinfo($jrxml, PATHINFO_DIRNAME) .'/'. pathinfo($jrxml, PATHINFO_FILENAME);
      $jasper->process(
        $jrxml,
  			$o,
        $fmt,
        $params,
        $args,
  			[],
  			false // do not use background
      )->execute();
      $report = $o .'.'. $fmt[0];
      $this->assertTrue(is_readable($report));
    }

}
